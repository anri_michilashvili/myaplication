package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.second_activity.*

class second_activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.second_activity)
        init()
    }
    private fun init(){
        Glide.with(this)
            .load("https://gamingcentral.in/wp-content/uploads/2017/10/OcJEZUB.jpg")
            .into(imageView1)

        Glide.with(this)
            .load("https://tse2.mm.bing.net/th?id=OIP.o1AaQFvhWlXbAkfS3iujmwHaEK&pid=Api&P=0&w=271&h=153")
            .into(profileImageView)
    }
}